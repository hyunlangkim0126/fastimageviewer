//
//  TilingLayer.m
//  NaverBODTest
//
//  Created by Hyunlang Kim on 2017. 2. 10..
//  Copyright © 2017년 Initoz. All rights reserved.
//

#import "TilingLayer.h"

@implementation TilingLayer

+ (CFTimeInterval)fadeDuration;
{
    return 0.01;
}

@end
