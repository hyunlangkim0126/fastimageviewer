//
//  ViewController.h
//  FastImageViewer
//
//  Created by Hyunlang Kim on 2017. 2. 15..
//  Copyright © 2017년 Waterdropnet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

+ (ViewController *)photoViewControllerForPageIndex:(NSUInteger)pageIndex;

- (NSInteger)pageIndex;

@end

