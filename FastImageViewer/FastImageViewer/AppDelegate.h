//
//  AppDelegate.h
//  FastImageViewer
//
//  Created by Hyunlang Kim on 2017. 2. 15..
//  Copyright © 2017년 Waterdropnet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder  <UIApplicationDelegate, UIPageViewControllerDataSource>

@property (strong, nonatomic) UIWindow *window;


@end

